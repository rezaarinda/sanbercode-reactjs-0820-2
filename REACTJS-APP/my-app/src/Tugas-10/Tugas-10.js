import React from 'react';
import '../App.css'

let dataHargaBuah = [
    {nama: "Semangka", harga: 10000, berat: 1000},
    {nama: "Anggur", harga: 40000, berat: 500},
    {nama: "Strawberry", harga: 30000, berat: 400},
    {nama: "Jeruk", harga: 30000, berat: 1000},
    {nama: "Mangga", harga: 30000, berat: 500}
  ]


class DaftarHarga extends React.Component {
    render() {
        return (
            <>
                {dataHargaBuah.map(el=> {
                    return (
                        <tr>
                            <td>{el.nama}</td>
                            <td>{el.harga}</td>
                            <td>{el.berat/1000} Kg</td>
                        </tr>
                    )
                })}
        </>
        )
    }
}

export default class Tabel extends React.Component{    
    render(){
        return (
            <div className="content">
                <h1 style = {{textAlign: "center"}}>Tabel Harga Buah</h1>
                <table>
                    <tr>
                        <th style={{width : "300px", backgroundColor: "#a3a3a3"}}>Nama</th>
                        <th style={{width : "200px", backgroundColor: "#a3a3a3"}}>Harga</th>
                        <th style={{width : "190px", backgroundColor: "#a3a3a3"}}>Berat</th>
                    </tr>
                    <DaftarHarga/>
                </table>
            </div>
        )
    }
}
